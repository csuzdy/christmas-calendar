# The Christmas Calendar project
We decided with our neighbours to create a Christmas Calendar. We used the windows of the house, we live in. Every day a window was decorated with the actual number of the day, from 1 to 24 of December. We choosed the number: 20.

# Technical details
I used my Raspberry Pi (model B) with an MCP23017 I/O expander to drive the 16 LED of the display through I2C.

## Outside
![Front](https://github.com/csuzdy/christmas-calendar/raw/master/front.jpg "The window from outside")

## Inside :)
![Back](https://github.com/csuzdy/christmas-calendar/raw/master/back.jpg "The window from inside")

## Video
https://drive.google.com/file/d/1Iw6q2GYzlfZoceIJylv7M2EjyrdhMGSE/view?usp=sharing
