#!/bin/python3
import smbus

BUS = smbus.SMBus(1)
ADDRESS = 0x20
DIRA = 0x00
DIRB = 0x01

FIRST_ROW_ADDRESS = 0x15
SECOND_ROW_ADDRESS = 0x14


def init():
    BUS.write_byte_data(ADDRESS, DIRA, 0x00)
    BUS.write_byte_data(ADDRESS, DIRB, 0x00)


def update(data):
    """Update the LEDs
       data: {
        'first': 'XXX___X_',
        'second': 'XXXXXXXX'
       }
    """
    firstnum = convert_to_num(data['first'])
    secondnum = convert_to_num(data['second'])
    BUS.write_byte_data(ADDRESS, FIRST_ROW_ADDRESS, firstnum)
    BUS.write_byte_data(ADDRESS, SECOND_ROW_ADDRESS, secondnum)


def convert_to_num(rowstr):
    """rowstr: X___X__X
       output: 10001001
       """
    if len(rowstr) != 8:
        raise ValueError("Length is not 8: " + rowstr)
    return int(rowstr.replace('X', '1').replace('_', '0'), 2)
