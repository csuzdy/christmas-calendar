#!/bin/python3
import interface
import time
from random import shuffle

DELAY = 0.2
ANIMATION_DELAY = 3

SHIFT = [
    {'first': 'X_______', 'second': 'X_______', 'sleep': DELAY},
    {'first': '_X______', 'second': '_X______', 'sleep': DELAY},
    {'first': '__X_____', 'second': '__X_____', 'sleep': DELAY},
    {'first': '___X____', 'second': '___X____', 'sleep': DELAY},
    {'first': '____X___', 'second': '____X___', 'sleep': DELAY},
    {'first': '_____X__', 'second': '_____X__', 'sleep': DELAY},
    {'first': '______X_', 'second': '______X_', 'sleep': DELAY},
    {'first': '_______X', 'second': '_______X', 'sleep': DELAY},
    {'first': 'X______X', 'second': 'X______X', 'sleep': DELAY},
    {'first': '_X_____X', 'second': '_X_____X', 'sleep': DELAY},
    {'first': '__X____X', 'second': '__X____X', 'sleep': DELAY},
    {'first': '___X___X', 'second': '___X___X', 'sleep': DELAY},
    {'first': '____X__X', 'second': '____X__X', 'sleep': DELAY},
    {'first': '_____X_X', 'second': '_____X_X', 'sleep': DELAY},
    {'first': '______XX', 'second': '______XX', 'sleep': DELAY},
    {'first': 'X_____XX', 'second': 'X_____XX', 'sleep': DELAY},
    {'first': '_X____XX', 'second': '_X____XX', 'sleep': DELAY},
    {'first': '__X___XX', 'second': '__X___XX', 'sleep': DELAY},
    {'first': '___X__XX', 'second': '___X__XX', 'sleep': DELAY},
    {'first': '____X_XX', 'second': '____X_XX', 'sleep': DELAY},
    {'first': '_____XXX', 'second': '_____XXX', 'sleep': DELAY},
    {'first': 'X____XXX', 'second': 'X____XXX', 'sleep': DELAY},
    {'first': '_X___XXX', 'second': '_X___XXX', 'sleep': DELAY},
    {'first': '__X__XXX', 'second': '__X__XXX', 'sleep': DELAY},
    {'first': '___X_XXX', 'second': '___X_XXX', 'sleep': DELAY},
    {'first': '____XXXX', 'second': '____XXXX', 'sleep': DELAY},
    {'first': 'X___XXXX', 'second': 'X___XXXX', 'sleep': DELAY},
    {'first': '_X__XXXX', 'second': '_X__XXXX', 'sleep': DELAY},
    {'first': '__X_XXXX', 'second': '__X_XXXX', 'sleep': DELAY},
    {'first': '___XXXXX', 'second': '___XXXXX', 'sleep': DELAY},
    {'first': 'X__XXXXX', 'second': 'X__XXXXX', 'sleep': DELAY},
    {'first': '_X_XXXXX', 'second': '_X_XXXXX', 'sleep': DELAY},
    {'first': '__XXXXXX', 'second': '__XXXXXX', 'sleep': DELAY},
    {'first': 'X_XXXXXX', 'second': 'X_XXXXXX', 'sleep': DELAY},
    {'first': '_XXXXXXX', 'second': '_XXXXXXX', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
]

TOP_BOTTOM = [
    {'first': '________', 'second': '________', 'sleep': DELAY},
    {'first': 'XX______', 'second': 'X_______', 'sleep': DELAY},
    {'first': 'XXX_____', 'second': 'XX_____X', 'sleep': DELAY},
    {'first': 'XXXX____', 'second': 'XXX___XX', 'sleep': DELAY},
    {'first': 'XXXXX___', 'second': 'XXXX_XXX', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
]

BOTTOM_TOP = [
    {'first': '________', 'second': '________', 'sleep': DELAY},
    {'first': '_____XXX', 'second': '____X___', 'sleep': DELAY},
    {'first': '____XXXX', 'second': '___XXX__', 'sleep': DELAY},
    {'first': '___XXXXX', 'second': '__XXXXX_', 'sleep': DELAY},
    {'first': '__XXXXXX', 'second': '_XXXXXXX', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
]

BLINK = [
    {'first': '________', 'second': '________', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
    {'first': '________', 'second': '________', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
    {'first': '________', 'second': '________', 'sleep': DELAY},
    {'first': 'XXXXXXXX', 'second': 'XXXXXXXX', 'sleep': DELAY},
]

def random():
    first_leds = list(range(8))
    second_leds = list(range(8))
    shuffle(first_leds)
    shuffle(second_leds)
    fl = list('_'*8)
    sl = list('_'*8)
    for letter in range(2):
        for i in range(8):
            data = {}
            if letter == 0:
                fl[first_leds[i]] = 'X'
            else:
                sl[second_leds[i]] = 'X'
            data['first'] = "".join(fl)
            data['second'] = "".join(sl)
            data['sleep'] = DELAY
            yield(data)


interface.init()

if __name__ == '__main__':
    while True:
        for animation in (SHIFT, TOP_BOTTOM, BOTTOM_TOP, BLINK, list(random())):
            for data in animation:
                interface.update(data)
                time.sleep(data['sleep'])
            time.sleep(ANIMATION_DELAY)
